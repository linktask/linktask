package com.our.edu.newsapp.utils

import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

abstract class BindingAdapter {

    companion object {
        @JvmStatic
        @BindingAdapter("loadImage:url")
        fun loadImage(imageView: ImageView, url: String?) {
            if (!url.isNullOrBlank())
                Picasso.get().load(url).into(imageView)
        }

        @JvmStatic
        @BindingAdapter("loadImage:resource")
        fun loadImage(imageView: ImageView, resource: Int) {
            imageView.setImageResource(resource)
        }

        @JvmStatic
        @BindingAdapter("recycler:adapter")
        fun recyclerAdapter(
            recyclerView: RecyclerView,
            adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>
        ) {
            recyclerView.adapter = adapter
        }

    }
}