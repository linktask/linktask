package com.our.edu.newsapp.utils


object Constants {

    /**************************
     *   BASE URL
     **************************/
    const val BASE_URL = "https://newsapi.org/v1/"


    /**************************
     *   constant variables
     **************************/
    const val API_KEY = "533af958594143758318137469b41ba9"

    const val ARGUMENT_KEY = "article"


    /**************************
     *   RecyclerView Animation
     **************************/
    const val ANIMATION_RISE_UP = 1
    const val ANIMATION_RIGHT_TO_LEFT = 2

}