package com.our.edu.newsapp.dataModel

import java.io.Serializable

class DomainNewsModel : Serializable {
    constructor(
        author: String?,
        description: String?,
        publishedAt: String?,
        title: String?,
        url: String?,
        urlToImage: String?
    ) {
        this.author = author
        this.description = description
        this.publishedAt = publishedAt
        this.title = title
        this.url = url
        this.urlToImage = urlToImage
    }

    var author: String? = null
    var description: String? = null
    var publishedAt: String? = null
    var title: String? = null
    var url: String? = null
    var urlToImage: String? = null

}