package com.our.edu.newsapp.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle


object IntentClass {

    // open activity with animation
    fun goToActivity(
        activity: Activity,
        target: Class<*>,
        value: Bundle? = null,
        enterAnim: Int = -1,
        exitAnim: Int = -1,
        clear: Boolean = false
    ) {
        val intent = Intent(activity, target)
        if (clear)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra("data", value)
        activity.startActivity(intent)
        if (enterAnim != -1 && exitAnim != -1)
            activity.overridePendingTransition(enterAnim, exitAnim)
    }

    // open activity without animation
    fun goToActivity(
        context: Context,
        target: Class<*>,
        value: Bundle? = null,
        clear: Boolean = false
    ) {
        val intent = Intent(context, target)
        if (clear)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra("data", value)
        context.startActivity(intent)
    }


    fun goToStartForResult(
        currentActivity: Activity,
        targetClass: Class<*>?,
        requestCode: Int,
        value: Bundle?
    ) {
        val intent = Intent(currentActivity, targetClass)
        intent.putExtra("data", value)
        currentActivity.startActivityForResult(intent, requestCode)
    }


    fun goToActivityAndClear(
        currentActivity: Context,
        targetClass: Class<*>?,
        value: Bundle?
    ) {
        val intent = Intent(currentActivity, targetClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.putExtra("data", value)
        currentActivity.startActivity(intent)
    }


    fun goToLink(activity: Activity, link: String) {
        if (link.isNotBlank()) {
            val uri = Uri.parse(link)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            activity.startActivity(intent)
        }
    }

}