package com.our.edu.newsapp.dataModel.error

import com.our.edu.newsapp.dataModel.error.Error

data class ErrorModel(
    val errors: List<Error>
)