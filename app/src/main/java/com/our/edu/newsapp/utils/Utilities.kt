package com.our.edu.newsapp.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.our.edu.newsapp.R
import es.dmoral.toasty.Toasty


object Utilities {

    fun hideKeyboard(view: View) {
        try {
            val imm =
                view.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun runAnimation(recyclerView: RecyclerView, type: Int) {
        val context: Context = recyclerView.context
        var controller: LayoutAnimationController? = null
        controller = if (type == Constants.ANIMATION_RISE_UP) {
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_rise_up)
        } else {
            AnimationUtils.loadLayoutAnimation(
                context,
                R.anim.layout_animation_slide_from_right
            )
        }
        recyclerView.layoutAnimation = controller
        recyclerView.scheduleLayoutAnimation()
    }

    fun snackBarMessage(view: View, message: String){

        Snackbar.make(view, message, Snackbar.LENGTH_LONG).setAction("DONE") {

            }.show()
    }

    fun toastyError(context: Context, message: String) {
        Toasty.error(context, message, Toast.LENGTH_SHORT, true).show()
    }

    fun toastySuccess(context: Context, message: String) {
        Toasty.success(context, message, Toast.LENGTH_SHORT, false).show()
    }

    fun logError(tag: String, message: String) {
        Log.e(tag, message)
    }

    fun logWarning(tag: String, message: String) {
        Log.w(tag, message)
    }

    // check network connectivity
    fun isConnected(getApplicationContext: Context): Boolean {
        var status = false
        val cm =
            getApplicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (cm.activeNetwork != null && cm.getNetworkCapabilities(cm.activeNetwork) != null) {
                // connected to the internet
                status = true
            }
        } else {
            if (cm.activeNetworkInfo != null && cm.activeNetworkInfo!!.isConnectedOrConnecting) {
                // connected to the internet
                status = true
            }
        }
        return status
    }

}
